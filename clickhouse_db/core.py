from clickhouse_driver import Client
import pandas as pd

from .utils import convert_types, escape


class ClientExtendedPandas:
    client = None

    def __init__(self, *args, **kwargs):
        _client = Client(*args, **kwargs)
        _client.execute('Select 1')
        self.__class__.client = _client

    @staticmethod
    def select(query: str, sort: str = None) -> pd.DataFrame:
        data, column_types = ClientExtendedPandas.client.execute(query, with_column_types=True)
        columns, types = zip(*column_types)
        types = convert_types(types, True)
        df = pd.DataFrame(data, columns=columns)
        df = df.astype(dtype=dict(zip(columns, types)), copy=False)

        if sort:
            df.sort_values(sort, inplace=True)
            df.reset_index(inplace=True, drop=True)

        return df

    @staticmethod
    def insert(df: pd.DataFrame, table: str, db: str = 'default'):
        columns = ', '.join(map(escape, df.columns))
        query = f'INSERT INTO {db}.{table} ({columns}) VALUES'
        return ClientExtendedPandas.client.execute(query, df.values.tolist(), types_check=True)

    @staticmethod
    def truncate(table, db='default'):
        ClientExtendedPandas.client.execute(f'TRUNCATE TABLE IF EXISTS {db}.{table}')

    @staticmethod
    def execute(query, **kwargs):
        return ClientExtendedPandas.client.execute(query, **kwargs)
